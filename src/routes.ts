export const ROUTE_PREFIX = 'api'
export const ROUTE_TEST = `${ROUTE_PREFIX}/test`

export const pre = (route: string) => {
    return `${ROUTE_PREFIX}/${route}`
}