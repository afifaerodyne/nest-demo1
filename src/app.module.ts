import { Module } from '@nestjs/common'

import { MongooseModule } from '@nestjs/mongoose'
import { StationsModule } from './components/stations/stations.module'

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/test'),
    StationsModule
  ]
})
export class AppModule { }
