/** Create Station DTO */

export class CreateStationDto {
    tag: string
    loc: [number, number]
    data: any
}