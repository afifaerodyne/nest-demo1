/** Stations schema test */

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type StationDocument = Station & Document

@Schema()
export class Station {
    /** Tagging on the station */
    @Prop()
    tag: string

    /** Location */
    @Prop()
    loc: [number, number]

    /** Any additional data */
    @Prop()
    data: any
}

export const StationSchema = SchemaFactory.createForClass(Station)
