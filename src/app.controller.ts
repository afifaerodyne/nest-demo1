import { Controller, Get, Res } from '@nestjs/common';
import { response } from 'express';
import path from 'path';
import { AppService } from './app.service';
import { ROUTE_TEST } from './routes';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  /** This is created based on a tutorial that combines angular and nest together */
  // @Get()
  // root(@Res() response): void {
  //   response.sendFile(path.resolve('../dist/index.html'));
  // }

  @Get(ROUTE_TEST)
  getHello(): string {
    return this.appService.getHello()
  }
}
