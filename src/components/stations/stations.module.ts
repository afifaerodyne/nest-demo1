import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { StationsService } from './stations.service'
import { StationsController } from './stations.controller'
import { Station, StationSchema } from 'src/schemas/station.schema'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Station.name, schema: StationSchema }])
  ],
  providers: [StationsService],
  controllers: [StationsController]
})
export class StationsModule { }
