import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common'
import { CreateStationDto } from 'src/dto/create-station.dto'
import { pre } from 'src/routes'
import { StationsService } from './stations.service'

@Controller(pre('stations'))
export class StationsController {
    constructor(
        private stationServ: StationsService
    ) { }

    @Get()
    findAll() {
        return this.stationServ.findAll()
    }

    @Post()
    create(@Body() createStationDto: CreateStationDto) {
        return this.stationServ.create(createStationDto)
    }

    @Get(':id')
    async findOne(@Param() params) {
        if (params?.id) {
            const get = await this.stationServ.findById(params.id)
            console.log('findOne().get: ', get)
            return get
        }
        else throw 'No parameter found'
    }

    @Put(':id')
    updateOne(@Body() createStationDto: CreateStationDto, @Param() params) {
        if (params?.id && createStationDto)
            return this.stationServ.update(params.id, createStationDto)
        else throw 'No parameter found'
    }

    @Delete(':id')
    deleteOne(@Param() params) {
        if (params?.id)
            return this.stationServ.delete(params.id)
        else throw 'No parameter found'
    }
}
