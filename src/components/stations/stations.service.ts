import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { CreateStationDto } from 'src/dto/create-station.dto'
import { Station, StationDocument } from 'src/schemas/station.schema'

@Injectable()
export class StationsService {
    constructor(
        @InjectModel(Station.name) private stationModel: Model<StationDocument>
    ) { }

    async create(createStationDto: CreateStationDto): Promise<Station> {
        const createdStat = new this.stationModel(createStationDto)

        return createdStat.save()
    }

    async findAll(): Promise<Station[]> {
        return this.stationModel.find().exec()
    }

    async findById(id: string): Promise<Station> {
        return this.stationModel.findById(id).exec()
    }

    /** 
     * Updates single station based on the ID provided 
     * @returns Document before it was updated. Refer: https://mongoosejs.com/docs/tutorials/findoneandupdate.html
     */
    async update(id: string, updateStationDto: CreateStationDto): Promise<Station> {
        return this.stationModel.findOneAndUpdate({ _id: id }, updateStationDto).exec()
    }

    /**
     * Delete a single station based on the ID provided
     * @param id ID of the station
     */
    async delete(id: string): Promise<Station> {
        return this.stationModel.findOneAndDelete({ _id: id }).exec()
    }
}
